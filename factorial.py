def factorial(n):
    mult = 1
    for i in range(n):
        mult *= i + 1

    return mult


print(factorial(2))
print(factorial(3))
print(factorial(10))
