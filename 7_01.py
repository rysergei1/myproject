def say_hello(name):
    print(f'Hello, {name}')

names = ['Sergey', 'Polina', 'Sava', 'Artyom', 'Kolya']

for name in names:
    say_hello(name)