summ = 0

while True:
    a = input('Введите число:\n')
    if a == 'stop':
        break
    a = int(a)
    if a % 5 == 0:
        continue

    summ += a
print('Сумма введенных чисел =', summ)