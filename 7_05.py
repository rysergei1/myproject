def calc(*args):
    summator = 0
    for i, item in enumerate(args):
        summator += i * item

    return summator


print(calc(1, 1, 1, 2, 3))
