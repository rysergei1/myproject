# search_in_matrix
def search_in_matrix(matrix, search_max=True):
    result = None

    for row in matrix:
        for item in row:
            if result is None:
                result = item
            elif search_max is True and item > result:
                result = item
            elif search_max is False and item < result:
                result = item

    return result