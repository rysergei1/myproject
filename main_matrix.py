# main_matrix
from create_matrix import create_matrix
from show import show_matrix
from sum_matrix import sum_matrix, sum_matrix_new
from search_in_matrix import search_in_matrix


matrix = create_matrix()
print(matrix)
show_matrix(matrix)
sum_1 = sum_matrix(matrix)
sum_2 = sum_matrix_new(matrix)
print('Sum 1:', sum_1, 'Sum 2:', sum_2)

max_elem = search_in_matrix(matrix)
max_elem_1 = search_in_matrix(matrix, search_max=True)
min_elem = search_in_matrix(matrix, search_max=False)
print('Max 1:', max_elem, '\nMax 2:', max_elem_1)
print('Min:', min_elem)





