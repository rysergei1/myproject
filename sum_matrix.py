# sum_matrix
def sum_matrix(matrix):
    summator = 0
    for row in matrix:
        for item in row:
            summator += item

    return  summator

def sum_matrix_new(matrix):
    summator = 0
    for row in matrix:
        summator += sum(row)

    return summator